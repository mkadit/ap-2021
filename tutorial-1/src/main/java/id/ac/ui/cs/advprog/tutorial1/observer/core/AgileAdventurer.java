package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

    public AgileAdventurer(Guild guild) {
        // Done: Complete Me
        this.name = "Agile";
        this.guild = guild;
    }

    // Done: Complete Me
    @Override
    public void update() {
        if (this.guild.getQuestType().equals("D") || this.guild.getQuestType().equals("R")) {
            this.getQuests().add(this.guild.getQuest());
        }
    }
}
