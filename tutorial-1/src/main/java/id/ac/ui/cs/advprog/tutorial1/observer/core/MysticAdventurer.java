package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

    public MysticAdventurer(Guild guild) {
        // Done: Complete Me
        this.name = "Mystic";
        this.guild = guild;
    }

    // Done: Complete Me
    @Override
    public void update() {

        if (this.guild.getQuestType().equals("D") || this.guild.getQuestType().equals("E")) {
            this.getQuests().add(this.guild.getQuest());
        }
    }
}
