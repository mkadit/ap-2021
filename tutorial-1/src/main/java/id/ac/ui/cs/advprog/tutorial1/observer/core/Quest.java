package id.ac.ui.cs.advprog.tutorial1.observer.core;

import java.util.List;

import id.ac.ui.cs.advprog.tutorial1.observer.core.Adventurer.AdventurerType;

public class Quest {
    private String title;
    private String type;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return this.title;
    }

    public String getType() {
        return this.type;
    }

}
