package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
    // Done: Complete me

    public KnightAdventurer() {
        setAttackBehavior(new AttackWithSword());
        setDefenseBehavior(new DefendWithArmor());

    }

    @Override
    public String getAlias() {
        return "Knight";

    }
}
