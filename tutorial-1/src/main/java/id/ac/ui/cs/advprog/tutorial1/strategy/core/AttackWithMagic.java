package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    // Done: Complete me

    @Override
    public String getType() {
        return "Magic";

    }

    @Override
    public String attack() {
        return "Magic goes brrr";

    }

}
